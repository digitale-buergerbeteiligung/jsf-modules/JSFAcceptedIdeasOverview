package beans;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.primefaces.context.RequestContext;

import com.liferay.faces.util.logging.Logger;
import com.liferay.faces.util.logging.LoggerFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import ideaService.model.Ideas;
import ideaService.service.IdeasLocalService;
import servicetrackers.IdeaLocalServiceTracker;
import servicetrackers.UserLocalServiceTracker;

@ManagedBean(name = "acceptedIdeasOverviewBean")
@SessionScoped
public class AcceptedIdeasOverviewBean implements Serializable {

	private static final long serialVersionUID = 8535498386024542280L;

	private static Logger logger;
	private List<Ideas> ideas;

	private IdeaLocalServiceTracker ideasLocalServiceTracker;
	private UserLocalServiceTracker userLocalServiceTracker;

	public void onPageLoad() {
		updateIdeas();
		logger.info("Ideas updated by " + this.getClass().getName());
	}

	@PostConstruct
	public void postConstruct() {
		Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		BundleContext bundleContext = bundle.getBundleContext();
		ideasLocalServiceTracker = new IdeaLocalServiceTracker(bundleContext);
		userLocalServiceTracker = new UserLocalServiceTracker(bundleContext);
		ideasLocalServiceTracker.open();
		userLocalServiceTracker.open();
		logger = LoggerFactory.getLogger(this.getClass().getName());
		logger.info(this.getClass().getName() + " initialized successfully");
	}

	@PreDestroy
	public void preDestroy() {
		ideasLocalServiceTracker.close();
		userLocalServiceTracker.close();
	}

	public void getIdeasInMultiplesOfSix(int mult) {
		int multiplied = 6 * mult;
		this.setIdeas(getIdeasSubList(multiplied, multiplied + 6));
		RequestContext.getCurrentInstance().update("ideaoverviewform:cardgroup");
	}

	private List<Ideas> getIdeasSubList(int from, int to) {
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		return safeSubList(ideasLocalService.getAllAccpetedIdeas(), from, to);
	}

	private <T> List<T> safeSubList(List<T> list, int fromIndex, int toIndex) {
		int size = list.size();
		if (fromIndex >= size || toIndex <= 0 || fromIndex >= toIndex) {
			return Collections.emptyList();
		}

		fromIndex = Math.max(0, fromIndex);
		toIndex = Math.min(size, toIndex);

		return list.subList(fromIndex, toIndex);
	}

	/**
	 * updates the current set of ideas.
	 */
	public void updateIdeas() {
		// setIdeas(getIdeasSubList(0,6));
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		setIdeas(ideasLocalService.getAllAccpetedIdeas());
	}

	public int getIdeasRatingCount(long id) {
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		return ideasLocalService.getIdeasRatingCount(id);
	}

	public boolean showPlaceholder(long id) {
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		for (Ideas i : ideas) {
			if (i.getPrimaryKey() == id && ideasLocalService.getPictureUrlByIdeasRefAndPosition(id, 0).equals("/")) {
				return true;
			}
		}
		return false;
	}

	public boolean showPicture(long id) {
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		for (Ideas i : ideas) {
			if (i.getPrimaryKey() == id && ideasLocalService.getPictureUrlByIdeasRefAndPosition(id, 0).equals("/")) {
				return false;
			}
		}
		return true;
	}

	public String divHeight() {
		int start = ideas.size();
		while (start % 3 != 0) {
			start++;
		}
		int res = (start / 3) * 500;

		return res + "px;";
	}

	public String getTitlePicture(long id) {
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		return ideasLocalService.getPictureUrlByIdeasRefAndPosition(id, 0);
	}

	
	public boolean isLoggedIn() {
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext externalContext = context.getExternalContext();
		if (!themeDisplay.isSignedIn()) {
//			try {
//				externalContext.redirect("/c/portal/login");
//			} catch (IOException e) {
//				logger.error("No redirect possible" , e);
//			}
			return false;
		}else{
			
			return true;
		}				
	}
	
	public boolean showVoteButton(long id) {
		if(this.isLoggedIn() && this.isIdeaInValidPeriod(id) && this.userCanVote(id))
			return true;
		
		if(!this.isLoggedIn() && this.isIdeaInValidPeriod(id))
			return true;
		
		return false;
	}
	
	private boolean userCanVote(long id) {
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		if (themeDisplay.isSignedIn()) {
			IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
			try {
				User user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
				String[] voterUsernames = ideasLocalService.getIdeas(id).getRating().split(",");
				for (String username : voterUsernames) {
					// user has already voted for this idea
					if (username.equals(user.getScreenName())) {
						return false;
					}
				}				
			} catch (Exception e) {
				logger.info(e + " --------------- AcceptedIdeasOverviewBean::userCanVote -------------");				
			}
			return true;
		} else {
			return false;
		}
	}

	private boolean isIdeaInValidPeriod(long id) {
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		try {			
			 // get current date			 
			 Date currentDate = new Date();
			
			//shift current date to the prev month (also changes year if month is JANUARY)
			Calendar calCurrent = Calendar.getInstance();
			calCurrent.setTime(currentDate);
			calCurrent.add(Calendar.MONTH, -1);
			currentDate = calCurrent.getTime();						 
			 
			//get current year
			int currentYear = this.getYearOfDate(currentDate);

			//get current month
			int currentMonth = this.getMonthOfDate(currentDate);

			Date createDateOfIdea = ideasLocalService.getIdeas(id).getCreateDate();
			
			//shift createDate of idea to previous month
			Calendar calIdea = Calendar.getInstance();
			calIdea.setTime(createDateOfIdea);
			calIdea.add(Calendar.MONTH, -1);
			createDateOfIdea = calIdea.getTime();			
			
			//get year of the idea
			int yearOfIdea = this.getYearOfDate(createDateOfIdea);
						
			//get month of idea in number and not in string
			int monthOfIdea = this.getMonthOfDate(createDateOfIdea);
			
			int[] ideaPeriod = this.getPeriod(monthOfIdea);
			
			int[] currentPeriod = this.getPeriod(currentMonth);

			//if the idea belongs to the current year and in the current period, then it's available for voting
			if(yearOfIdea == currentYear && this.samePeriods(ideaPeriod, currentPeriod)) {
				logger.info(" ===== IDEA with primary key" + ideasLocalService.getIdeas(id).getPrimaryKey() + " IS VALID FOR VOTING ====");
				return true;
			}
		} catch (Exception e) {
			logger.info(" ===== isIdeaInValidPeriod ====");
			e.printStackTrace();
		}
		
		try {
			logger.info(" ===== IDEA  with primary key" + ideasLocalService.getIdeas(id).getPrimaryKey() + " IS ***NOT*** VALID FOR VOTING ====");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private int getYearOfDate(Date date) {		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int yearOfIdea = cal.get(Calendar.YEAR);
		return yearOfIdea;
	}
	
	private int getMonthOfDate(Date date) {
		LocalDate localIdeaDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int monthOfIdea = localIdeaDate.getMonthValue();
		return monthOfIdea;
	}
	/**
	 * Return the period of the given month. The months of the year are split by two, defining one period.
	 * @param month The month to be tested
	 * @return The period that the month belongs to
	 */
	private int[] getPeriod(int month) {
		Map<String,int[]> monthPeriods = new HashMap<String, int[]>();
		monthPeriods.put("JAN_FEB", new int[]{1,2});
		monthPeriods.put("MAR_APR", new int[]{3,4});
		monthPeriods.put("MAY_JUN", new int[]{5,6});
		monthPeriods.put("JUL_AUG", new int[]{7,8});
		monthPeriods.put("SEPT_OCT", new int[]{9,10});
		monthPeriods.put("NOV_DEC", new int[]{11,12});
		
		int[] period = new int[]{};
		switch (month) {
			case 1: 
				period = monthPeriods.get("JAN_FEB");
				break;
			case 2: 
				period = monthPeriods.get("JAN_FEB");
				break;
			case 3: 
				period = monthPeriods.get("MAR_APR");
				break;
			case 4: 
				period = monthPeriods.get("MAR_APR");
				break;
			case 5: 
				period = monthPeriods.get("MAY_JUN");
				break;
			case 6: 
				period = monthPeriods.get("MAY_JUN");
				break;
			case 7: 
				period = monthPeriods.get("JUL_AUG");
				break;
		   case 8:  
			   period = monthPeriods.get("JUL_AUG");
			   break;
		   case 9:  
			   period = monthPeriods.get("SEPT_OCT");
			   break;
		   case 10: 
			   period = monthPeriods.get("SEPT_OCT");
			   break;
		   case 11: 
			   period = monthPeriods.get("NOV_DEC");
			   break;
		   case 12: 
			   period = monthPeriods.get("NOV_DEC");
			   break;
		   default:
			   break;
		}
		return period;
	}
	
	/**
	 * Compare if the months of the two periods are the same.
	 * @return true if periods are the same, false otherwise
	 */
	private boolean samePeriods(int[] period1, int[] period2) {
		if(period1[0] == period2[0] && period1[1] == period2[1]) {
			return true;
		}
		logger.info("Idea period and current period ***DON'T*** MATCH");
		return false;
	}
	
	public void likeIdea(long id) {
		if(!isLoggedIn()) {
			logger.info("------- User not logged in ------- ");
			FacesContext context = FacesContext.getCurrentInstance();
			ExternalContext externalContext = context.getExternalContext();
			try {
				externalContext.redirect("/c/portal/login");
			} catch (Exception e1) {
				logger.error("could not redirect to login page", e1);
			}
		} else {
			// logger.info(Boolean.toString(userCanVote(id)));
			IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
			try {
				User user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
				boolean res = ideasLocalService.addUserToRating(user, id);
				// if(!res){
				// //ideasLocalService.removeUserFromRating(user, id);
				// }
				logger.info(user.getScreenName() + " has liked idea with primary key " + id);
			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				ExternalContext externalContext = context.getExternalContext();
				try {
					externalContext.redirect("/c/portal/login");
				} catch (Exception e1) {
					logger.error("could not redirect to login page", e1);
				}
			}
		}
	}

	private User getCurrentUser(String id) throws NumberFormatException, PortalException {
		UserLocalService userLocalService = userLocalServiceTracker.getService();
		return userLocalService.getUserById(Long.parseLong(id));
	}

	/**
	 * @return the ideas
	 */
	public List<Ideas> getIdeas() {
		return ideas;
	}

	/**
	 * @param ideas
	 *            the ideas to set
	 */
	public void setIdeas(List<Ideas> ideas) {
		this.ideas = ideas;
	}

}
